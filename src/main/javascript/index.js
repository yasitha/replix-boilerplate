import {firstController} from './controllers/firstController.js';
import {secondController} from './controllers/secondController.js';
import {ReplixApp} from 'replix-mvc';

ReplixApp.run();

