import {controller} from 'replix-mvc';
import {home} from 'replix-mvc';
import {homelayout} from '../views/controllerCallerView';
import { asyncAwaitService } from "../services/asyncAwaitService.js";
@controller("first")
class FirstController{

    @home("first")
    methodOne()
    {
        console.log("method one get calledd");
        asyncAwaitService.testFunction();
        return homelayout;
    }
    methodTwo()
    {
        console.log("method two get calledd");
    }
    dynamicparams(param1,param2)
    {
        console.log("dynamic params method gets called--------- ",param1,param2);
        return homelayout;
    }

}
export default FirstController;