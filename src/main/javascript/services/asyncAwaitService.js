import * as jq from "jquery";
class AsyncAwaitService{
    async testFunction(){
        console.log("testFunction");
        var data = await jq.get( "https://reqres.in/api/users?page=2");
        console.log("testFunction data " ,data);
    }
}
export let asyncAwaitService = new AsyncAwaitService();