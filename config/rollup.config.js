import babel from "rollup-plugin-babel";
import copy from 'rollup-plugin-copy';
import postprocess from 'rollup-plugin-postprocess';
import wildcardExternal from '@oat-sa/rollup-plugin-wildcard-external';

export default[
    {
        input:"src/main/javascript/components/main.js",
        output:{
            name:"reuseble",
            file:"dist/src/lib.js",
            format:"es" 
        },
        external:["react","react-dom",'../css/**','../../../src/main/resources/replix.properties.js','jquery','jquery-ui/ui/widgets/resizable'],
        plugins:[
            
            babel({
                exclude:"node_modules/**"
            }),
            copy({
                targets: [
                  { src: 'src/main/javascript/components/css', dest: 'dist/' },
                  { src: 'src/main/javascript/components/index.js', dest: 'dist/' },
                  { src: 'src/main/javascript/components/package.json', dest: 'dist/' },
                  { src: 'dist/*', dest: 'libs/' },
                  { src: 'src/main/javascript/components/css', dest: 'libs/' },
                  { src: 'dist/*', dest: 'node_modules/replix-commons/' },
                  { src: 'src/main/javascript/components/css', dest: 'node_modules/replix-commons/' },
                  { src: 'src/main/javascript/components/index.js', dest: 'node_modules/replix-commons/' },
                  { src: 'src/main/javascript/components/index.js', dest: 'node_modules/replix-commons/' },

                  
                ],
                hook: 'writeBundle'
              }),
              wildcardExternal([
                // Exclude all module from lib alias
                '../css/**'
            ])
              /*,
            postprocess([
                /*[/\/src\/main/, './src/main'],
                [/import indirect/, 'import {indirect}']               
            ])
            */

        ]
    }
];