var path = require('path');
var webpack = require('webpack');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');
var OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
var TerserPlugin = require ("terser-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');

module.exports={
mode:"production",
//mode:"development",
//define entry point
entry:['regenerator-runtime/runtime','./src/main/javascript/index.js'],

//define output
output:{
    path:path.resolve(__dirname,'../')+"/dist",
    publicPath:"/",
    filename:'js/build.js'
},
devServer: {
    contentBase: path.join('dist'),
    compress: true,
    port: 3000
  },
  module:{
    rules:
    [
       {
           test:/\.js$/,
           exclude:/(node_modules)/,
           loader:'babel-loader',
           query:
           {
               plugins: [["@babel/plugin-proposal-decorators", { "legacy": true }]],
               presets:['@babel/react','@babel/preset-env']
               
           }
       },
       {
         test: /\.s?css$/,
         oneOf: [
           {
             test: /\.module\.s?css$/,
             use: [
               MiniCssExtractPlugin.loader,
               {
                 loader: "css-loader",
                 options: { modules: true, exportOnlyLocals: false }
               },
               "sass-loader"
             ]
           },
           {
             use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
           }
         ]
       },
    ]
   },
optimization:{
    minimizer:[
        new OptimizeCssAssetsPlugin(),new TerserPlugin()
    ]
},
plugins:[
    new MiniCssExtractPlugin({filename:"css/main.css"}),
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        "window.jQuery":"jquery",
        'global.jQuery': 'jquery'
      }),
      new CopyPlugin([
        { from: './src/main/resources/html/', to: './' },
        { from: './src/main/resources/images/', to: './images' },
        { from: './src/main/resources/css/bootstrap-reboot.min.css', to: './css' }
      ])
]

};
