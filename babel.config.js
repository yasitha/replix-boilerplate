const presets = [
    [
        "@babel/env",
        {
            modules:false
        }
    ],
    ["@babel/preset-react"]
];
const plugins = [ 
    ["@babel/plugin-proposal-decorators", { "legacy": true }]
];
module.exports = {presets,plugins};